/*
 * consumerProducer.h
 *
 *  Created on: 21/04/2013
 *      Author: utnso
 */

#ifndef CONSUMERPRODUCER_H_
#define CONSUMERPRODUCER_H_

void consume(void);
void produce(void);
void algorithm(void);

#endif /* CONSUMERPRODUCER_H_ */
