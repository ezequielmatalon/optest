/*
 * personaje.c
 *
 *  Created on: 17/04/2013
 *      Author: utnso
 */

#include <commons/config.h>
#include <commons/collections/dictionary.h>
#include <stdio.h>
#include <stdlib.h>
#include "personaje.h"
#include <commons/string.h>
#include <string.h>
#include <commons/log.h>

//Si falla la lectura del archivo de configuracion de alguna propiedad
//el campo errores se pone en 1
t_personaje * crearPersonaje(char * path) {
	t_personaje *pers = malloc(sizeof(t_personaje));
	pers->errores=0;
	t_config *cfg = config_create(path);
	if (config_has_property(cfg, "nombre")) {
		pers->nombre = config_get_string_value(cfg, "nombre");
	} else {
		pers->errores = 1;
	}

	if (config_has_property(cfg, "planDeNiveles")) {
		pers->planDeNiveles = config_get_array_value(cfg, "planDeNiveles");
	} else {
		pers->errores = 1;
	}

	if (config_has_property(cfg, "simbolo")) {
		char *a = config_get_string_value(cfg, "simbolo");
		pers->simbolo = a[0];
	} else {
		pers->errores = 1;
	}

	if (config_has_property(cfg, "planDeNiveles")) {
		char * obj;

		void elClosure(char * string) {
			obj = malloc(strlen("obj") + 1);
			strcpy(obj, "obj");
			string_append_with_format(&obj, "[%s]", string);
			if (config_has_property(cfg, obj)) {
				char ** objetivos = config_get_array_value(cfg, obj);
				dictionary_put(pers->objetivos, string, objetivos);
			} else {
				pers->errores = 1;
			}

			free(obj);

		}
		pers->objetivos = dictionary_create();
		string_iterate_lines(pers->planDeNiveles, elClosure);

	}

	if(config_has_property(cfg,"vidas")){
		pers->vidas = config_get_int_value(cfg, "vidas");
	}else {
		pers->errores = 1;
	}

	if(config_has_property(cfg,"orquestador")){
		pers->orquestador = config_get_string_value(cfg, "orquestador");
	}else {
		pers->errores = 1;
	}

	return pers;
}
void freeString(char * string) {
	free(string);
}

void freeListaStrings(char ** lista) {
	string_iterate_lines(lista, freeString);
}

void freePersonaje(t_personaje *personaje) {
	free(personaje->nombre);
	dictionary_destroy_and_destroy_elements(personaje->objetivos,
			freeListaStrings);

	free(personaje->orquestador);

}

void loggearMovimiento(t_personaje * personaje, t_log * logger){
	char* mensaje = string_from_format("Personaje: %s Moviendose a (X,Y): (%d,%d)",personaje->nombre,personaje->posX,personaje->posY);

	log_info(logger,mensaje);
}

void iniciarNivel(t_personaje * personaje){
	personaje->posX=0;
	personaje->posY=0;
}
