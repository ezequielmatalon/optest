/*
 ============================================================================
 Name        : LetMeBeC.c
 Author      : Ezequiel Matalon
 Version     :
 Copyright   : GNU/GPL
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <commons/temporal.h>
#include <commons/config.h>
#include <string.h>
#include <commons/log.h>
#include <commons/collections/dictionary.h>
#include <commons/string.h>
#include "personaje.h"

t_log * logger;
static t_log_level logLevel=LOG_LEVEL_INFO;



void mostrar(char* key,void* value){
	char * valor = (char *)value;

	printf("Esta es la key %s y este el valor %s\n",key,valor);
}

void crearLogger(char * path){
	logger=log_create(path,"Super Mario Proc",1,logLevel);

}


int main(void) {
	char * a = malloc(strlen("/home/utnso/Escritorio/Nuevo")+1);
	strcpy(a,"/home/utnso/Escritorio/Nuevo");
	t_personaje * pers= crearPersonaje(a);
	iniciarNivel(pers);
	crearLogger("/home/utnso/Escritorio/Log");
	loggearMovimiento(pers,logger);
	if(pers->errores){
		printf("La configuracion tiene errores");
	}
	freePersonaje(pers);

	return EXIT_SUCCESS;
}

