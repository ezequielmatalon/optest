/*
 * personaje.h
 *
 *  Created on: 17/04/2013
 *      Author: utnso
 */

#ifndef PERSONAJE_H_
#define PERSONAJE_H_

#include <commons/collections/dictionary.h>
#include <commons/log.h>
typedef struct {
	char * nombre;
	char simbolo;
	t_dictionary* objetivos;
	char ** planDeNiveles;
	int vidas;
	char * orquestador;
	int errores;
	int posX;
	int posY;
}t_personaje;

t_personaje* crearPersonaje(char * path);
void freePersonaje(t_personaje *personaje);
void loggearMovimiento(t_personaje * personaje, t_log * logger);
void iniciarNivel(t_personaje * personaje);


#endif /* PERSONAJE_H_ */
