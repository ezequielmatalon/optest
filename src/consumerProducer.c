/*
 ============================================================================
 Name        : consumerProducer.c
 Author      : Ezequiel Matalon
 Version     :
 Copyright   : GNU/GPL
 Description : Algoritmo productor consumidor Referencia:
 http://en.wikipedia.org/wiki/Producer-consumer_problem
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <commons/collections/queue.h>
#include <semaphore.h>


t_queue *queu;
pthread_mutex_t lock;
pthread_t tid[2];
sem_t vacio;

char * palabras[10] = { "que", "mal", "la", "estoy", "pasando", "con",
		"operativos", "la", "pucha", "che" };




void consume(void) {
	char * consumido;
	int isEmpty;
	while (1) {


		sem_wait(&vacio);
			consumido = (char *) queue_pop(queu);
			printf("\nConsumi %s y hay %d elementos en la cola", consumido,
					queue_size(queu));






	}
}

void produce(void) {
	int random;
	char * palabra;
	while (1) {


			random = rand() % 10;
			palabra = palabras[random];
			queue_push(queu, palabra);
			printf("\nProduje %s y hay %d elementos en la cola", palabra,
					queue_size(queu));
			sem_post(&vacio);



	}

}


void algorithm(void) {

	queu = queue_create();
	sem_init(&vacio, 0, 0);

	if (pthread_mutex_init(&lock, NULL ) != 0) {
		printf("\n mutex init failed\n");
		return;
	}

	int err = pthread_create(&(tid[0]), NULL, &consume, NULL );
	if (err != 0) {
		printf("\ncan't create thread consume:[%s]", strerror(err));
		return;
	}

	err = pthread_create(&(tid[1]), NULL, &produce, NULL );
	if (err != 0) {
		printf("\ncan't create thread produce:[%s]", strerror(err));
		return;
	}

	pthread_join(tid[0], NULL );
	pthread_join(tid[1], NULL );


	pthread_mutex_destroy(&lock);

}
